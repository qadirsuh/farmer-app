package com.farmer.app;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static com.farmer.app.Utils.KEY_ADMIN_APP;

public class SplashScreenActivity extends AppCompatActivity {

    private TextView txtAppName;

    private String appNameSelectedFirstTime;

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    final FirebaseDatabase database = FirebaseDatabase.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        txtAppName = findViewById(R.id.txtAppName);

        appNameSelectedFirstTime = Utils.getSharedPref(Utils.KEY_APP_TYPE);

        Log.e("appNameSelected", appNameSelectedFirstTime + "");

        if (appNameSelectedFirstTime.equals(Utils.KEY_FARMER_APP)) {
            txtAppName.setText("Farmer App");
        } else if (appNameSelectedFirstTime.equals(Utils.KEY_ADMIN_APP)) {
            txtAppName.setText("Admin App");
        } else {
            txtAppName.setVisibility(View.GONE);
        }

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                if (appNameSelectedFirstTime.equals(Utils.KEY_FARMER_APP)) {
                    startActivity(new Intent(SplashScreenActivity.this, FarmerLoginActivity.class));
                    finish();
                } else if (appNameSelectedFirstTime.equals(KEY_ADMIN_APP)) {
                    startActivity(new Intent(SplashScreenActivity.this, AdminLoginActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(SplashScreenActivity.this, AppSelectionActivity.class));
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

}

