package com.farmer.app;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import io.fabric.sdk.android.Fabric;

public class App extends Application {

    public static App sInstance;
    private static FirebaseDatabase database;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(App.this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;
        FirebaseApp.initializeApp(this);
        Fabric.with(this);

        // init database
        database = FirebaseDatabase.getInstance();
    }

    public DatabaseReference getDbRoot() {
        return database.getReference("farmerAppDb");
    }
}
