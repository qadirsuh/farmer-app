package com.farmer.app.models;

import java.io.Serializable;

public class ChatUsers implements Serializable {

    private String displayName;
    private long lastModified;
    private String userName;

    public ChatUsers() {
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
