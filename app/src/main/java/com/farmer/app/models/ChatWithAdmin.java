package com.farmer.app.models;

import java.io.Serializable;

public class ChatWithAdmin implements Serializable {

    private long createdAt;
    private String createdByUser;
    private boolean isAdmin;
    private String message;
    private String key;

    public ChatWithAdmin() {
    }

    public ChatWithAdmin(long createdAt, String createdByUser, boolean isAdmin, String message, String key) {
        this.createdAt = createdAt;
        this.createdByUser = createdByUser;
        this.isAdmin = isAdmin;
        this.message = message;
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        this.createdByUser = createdByUser;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
