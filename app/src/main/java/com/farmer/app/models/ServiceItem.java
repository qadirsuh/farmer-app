package com.farmer.app.models;

import java.io.Serializable;

public class ServiceItem implements Serializable {

    private String title;
    private String content;
    private Long createdAt;
    private String createdByUser;
    private String service;
    private String key;

    public ServiceItem() {
    }

    public ServiceItem(String title, String content, Long createdAt, String createdByUser, String service, String key) {
        this.title = title;
        this.content = content;
        this.createdAt = createdAt;
        this.createdByUser = createdByUser;
        this.service = service;
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public String getCreatedByUser() {
        return createdByUser;
    }

    public String getService() {
        return service;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String firebaseKey){
        this.key = firebaseKey;
    }
}
