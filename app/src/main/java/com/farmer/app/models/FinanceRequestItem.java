package com.farmer.app.models;

import java.io.Serializable;

public class FinanceRequestItem implements Serializable {

    private long createdAt;
    private String downloadUrl;
    private String createdByUser;
    private String filePath;
    private String message;
    private String subject;
    private String status;
    private String key;

    public String getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        this.createdByUser = createdByUser;
    }

    public FinanceRequestItem() {
    }

    public FinanceRequestItem(long createdAt, String downloadUrl, String filePath, String message, String subject, String status, String key) {
        this.createdAt = createdAt;
        this.downloadUrl = downloadUrl;
        this.filePath = filePath;
        this.message = message;
        this.subject = subject;
        this.status = status;
        this.key = key;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
