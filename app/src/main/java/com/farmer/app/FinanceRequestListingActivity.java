package com.farmer.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.farmer.app.adapters.FinanceItemsListAdapter;
import com.farmer.app.models.FinanceRequestItem;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;

public class FinanceRequestListingActivity extends AppCompatActivity {

    private Intent mIntent;
    private ListView serviceListView;
    private TextView txtHeader;

    private ProgressDialog mProgressDialog;

    private String selectedService;
    private String previousActivity;

    private Query uidRef;
    private ValueEventListener valueEventListener;

    private String farmerIdToFinanceRequests;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finance_request_listing);

        mProgressDialog = Utils.progressDialog("Loading...", this);

        serviceListView = findViewById(R.id.financeRequestListView);
        txtHeader = findViewById(R.id.txtHeader);

        mIntent = getIntent();
        selectedService = mIntent.getStringExtra("SELECTED_SERVICE");
        previousActivity = mIntent.getStringExtra("PREVIOUS_ACTIVITY");

        FloatingActionButton fab = findViewById(R.id.fab);

        if (previousActivity.equalsIgnoreCase(AdminLoginActivity.class.getSimpleName())) {
            fab.hide();
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent mIntent = new Intent(FinanceRequestListingActivity.this, FarmerFinanceRequestActivity.class);
                mIntent.putExtra("SELECTED_SERVICE", selectedService);
                mIntent.putExtra("PREVIOUS_ACTIVITY", previousActivity);
                startActivity(mIntent);

            }
        });

        if (Utils.getSharedPref(Utils.KEY_APP_TYPE).equalsIgnoreCase(Utils.KEY_ADMIN_APP)) {
            farmerIdToFinanceRequests = mIntent.getStringExtra("CHAT_WITH_USER_ID");
            Log.e("farmerIdToChatWith", farmerIdToFinanceRequests);
            String farmerDisplayName = mIntent.getStringExtra("USER_DISPLAY_NAME");
            txtHeader.setText("Finance Requests of \"" + farmerDisplayName + "\"");
        } else {
            farmerIdToFinanceRequests = Utils.getSharedPref(Utils.KEY_LOGIN_USER_NAME);
        }

        // Call get finance requests
        getAllFinanceRequests();
    }

    private void getAllFinanceRequests() {

        mProgressDialog.show();

        final DatabaseReference servicePath = App.sInstance.getDbRoot().child("financeRequests/" + farmerIdToFinanceRequests + "");
        uidRef = servicePath.orderByKey();
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mProgressDialog.dismiss();

                ArrayList<FinanceRequestItem> mFinanceRequestItems = new ArrayList<>();

                try {

                    if (dataSnapshot.getChildrenCount() == 0) {
                        Toast.makeText(FinanceRequestListingActivity.this, "No items found!", Toast.LENGTH_SHORT).show();
                    }

                    Log.e("dataSnapshot count", dataSnapshot.getChildrenCount() + "");

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        FinanceRequestItem financeRequestItem = postSnapshot.getValue(FinanceRequestItem.class);
                        financeRequestItem.setKey(postSnapshot.getKey());
                        Log.e("financeRequestItem", financeRequestItem.getSubject());
                        mFinanceRequestItems.add(financeRequestItem);
                    }

                    Collections.reverse(mFinanceRequestItems);

                    boolean isAdmin = false;
                    if (previousActivity.equalsIgnoreCase(AdminLoginActivity.class.getSimpleName())) {
                        isAdmin = true;
                    }

                    FinanceItemsListAdapter mItemsListAdapter = new FinanceItemsListAdapter(FinanceRequestListingActivity.this,
                            mFinanceRequestItems, farmerIdToFinanceRequests, isAdmin);
                    serviceListView.setAdapter(mItemsListAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("onCancelled", databaseError.getMessage()); // Log errors
            }
        };
        uidRef.addValueEventListener(valueEventListener);
    }
}
