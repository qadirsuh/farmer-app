package com.farmer.app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.farmer.app.adapters.UserListReCyclerViewAdapter;
import com.farmer.app.models.ChatUsers;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ChatUsersListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private UserListReCyclerViewAdapter adapter;

    private ArrayList<ChatUsers> chatUsers = new ArrayList<>();

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_users_list);

        mProgressDialog = Utils.progressDialog("Loading...", this);
        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                layoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        recyclerView.setLayoutManager(layoutManager);

        adapter = new UserListReCyclerViewAdapter(chatUsers);
        recyclerView.setAdapter(adapter);

        // Fetch messages from firebase database
        getChatList();
    }


    private void getChatList() {

        mProgressDialog.show();

        final DatabaseReference adminChatPath = App.sInstance.getDbRoot().child("chatWithAdmin/chatListUser");
        Query uidRef = adminChatPath.orderByKey();
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mProgressDialog.dismiss();
                chatUsers.clear();

                try {

                    if (dataSnapshot.getChildrenCount() == 0) {
                        Toast.makeText(ChatUsersListActivity.this, "Chat list is empty!", Toast.LENGTH_SHORT).show();
                    }

                    Log.e("dataSnapshot count", dataSnapshot.getChildrenCount() + "");

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        ChatUsers mChatUser = postSnapshot.getValue(ChatUsers.class);
                        Log.e("mChatUser", mChatUser.getDisplayName());
                        chatUsers.add(mChatUser);
                    }

                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("onCancelled", databaseError.getMessage()); // Log errors
            }
        };
        uidRef.addValueEventListener(valueEventListener);
    }

}
