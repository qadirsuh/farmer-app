package com.farmer.app;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class FarmerRegisterActivity extends AppCompatActivity {

    // Declaration EditTexts
    private EditText editTextFullName;
    private EditText editTextAge;
    private EditText editTextLocation;
    private EditText editTextContactNumber;
    private EditText editTextExperience;
    private EditText editTextUserName;
    private EditText editTextPassword;

    // Declaration Button
    private Button buttonRegister;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_register);

        initViews();
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String fullName = editTextFullName.getText().toString();
                String age = editTextAge.getText().toString();
                String location = editTextLocation.getText().toString();
                String contactNo = editTextContactNumber.getText().toString();
                String experience = editTextExperience.getText().toString();
                String userName = editTextUserName.getText().toString();
                String password = editTextPassword.getText().toString();

                if (fullName.isEmpty()) {
                    editTextFullName.setError("*Required field");
                } else if (age.isEmpty()) {
                    editTextAge.setError("*Required field");
                } else if (location.isEmpty()) {
                    editTextLocation.setError("*Required field");
                } else if (contactNo.isEmpty()) {
                    editTextContactNumber.setError("*Required field");
                } else if (experience.isEmpty()) {
                    editTextExperience.setError("*Required field");
                } else if (userName.isEmpty()) {
                    editTextUserName.setError("*Required field");
                } else if (password.isEmpty()) {
                    editTextPassword.setError("*Required field");
                } else {

                    Map<String, Object> farmerData = new HashMap<>();
                    farmerData.put("fullName", fullName);
                    farmerData.put("age", age);
                    farmerData.put("location", location);
                    farmerData.put("contactNo", contactNo);
                    farmerData.put("experience", experience);
                    farmerData.put("userName", userName);
                    farmerData.put("password", password);
                    farmerData.put("createdAt", System.currentTimeMillis());

                    registerNewUser(farmerData);

                }
            }
        });
    }

    // this method is used to connect XML views to its Objects
    private void initViews() {

        mProgressDialog = Utils.progressDialog("Loading...", this);
        editTextFullName = (EditText) findViewById(R.id.editTextFullName);
        editTextAge = (EditText) findViewById(R.id.editTextAge);
        editTextLocation = (EditText) findViewById(R.id.editTextLocation);
        editTextContactNumber = (EditText) findViewById(R.id.editTextContactNumber);
        editTextExperience = (EditText) findViewById(R.id.editTextExperience);
        editTextUserName = (EditText) findViewById(R.id.editTextUserName);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        buttonRegister = (Button) findViewById(R.id.buttonRegister);

    }

    private void registerNewUser(final Map<String, Object> farmerData) {

        mProgressDialog.show();
        // Check in the database is there any user associated with  this username
        final DatabaseReference ref = App.sInstance.getDbRoot().child("farmerUsers/" + farmerData.get("userName").toString());
        Log.e("get path", ref.getPath().toString());
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mProgressDialog.dismiss();

                try {

                    if (dataSnapshot.getValue() == null) {
                        // User doesn't exists already
                        // Register new user
                        ref.setValue(farmerData);
                        Toast.makeText(FarmerRegisterActivity.this, "Registration successful", Toast.LENGTH_LONG).show();
                        finish();

                    } else {
                        // User already exists
                        Toast.makeText(FarmerRegisterActivity.this, "Username not available. Please choose another one", Toast.LENGTH_LONG).show();
                        editTextUserName.setError("Username not available");
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("onCancelled", databaseError.getMessage()); // Log errors
            }
        };
        ref.addListenerForSingleValueEvent(valueEventListener);
    }
}
