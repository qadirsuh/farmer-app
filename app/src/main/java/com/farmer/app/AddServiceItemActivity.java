package com.farmer.app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class AddServiceItemActivity extends AppCompatActivity {

    private Intent mIntent;


    private EditText txt_title;
    private EditText txt_content;
    private Button btn_add;

    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service_item);

        mProgressDialog = Utils.progressDialog("Loading...", this);

        txt_title = findViewById(R.id.txt_title);
        txt_content = findViewById(R.id.txt_content);
        btn_add = findViewById(R.id.btn_add);

        mIntent = getIntent();
        final String selectedService = mIntent.getStringExtra("SELECTED_SERVICE");

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn_add.setEnabled(false);
                String title = txt_title.getText().toString();
                String content = txt_content.getText().toString();

                if (title.isEmpty()) {
                    txt_title.setError("* Required field");
                } else if (content.isEmpty()) {
                    txt_content.setError("* Required field");
                } else {

                    String service = selectedService.replace(" ", "_");

                    Log.e("service", service);

                    Map<String, Object> newTaskData = new HashMap<>();
                    newTaskData.put("createdAt", System.currentTimeMillis());
                    newTaskData.put("service", service);
                    newTaskData.put("title", title);
                    newTaskData.put("createdByUser", Utils.getSharedPref(Utils.KEY_LOGIN_USER_NAME));
                    newTaskData.put("content", content);

                    addServiceItemDb(service, newTaskData);
                }
            }
        });
    }

    private void addServiceItemDb(final String selectedService, final Map<String, Object> data) {

        final DatabaseReference servicePath = App.sInstance.getDbRoot().child("adminServices/" + selectedService + "");
        DatabaseReference newServiceId = servicePath.push();
        newServiceId.setValue(data);

        // String ticketKey = newTaskId.getKey();
        Toast.makeText(AddServiceItemActivity.this, "Success", Toast.LENGTH_SHORT).show();
        finish();
    }
}
