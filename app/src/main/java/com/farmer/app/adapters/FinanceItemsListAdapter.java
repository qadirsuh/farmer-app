package com.farmer.app.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.farmer.app.App;
import com.farmer.app.R;
import com.farmer.app.Utils;
import com.farmer.app.ViewFinanceRequestItemActivity;
import com.farmer.app.ViewServiceItemActivity;
import com.farmer.app.models.FinanceRequestItem;
import com.farmer.app.models.ServiceItem;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FinanceItemsListAdapter extends ArrayAdapter<FinanceRequestItem> {

    private String farmerId;
    private boolean isAdmin;
    private ProgressDialog mProgressDialog;

    public FinanceItemsListAdapter(@NonNull Context context, ArrayList<FinanceRequestItem> financeRequestItems, String farmerId, boolean isAdmin) {
        super(context, 0, financeRequestItems);
        this.farmerId = farmerId;
        this.isAdmin = isAdmin;
        mProgressDialog = Utils.progressDialog("Loading...", context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final FinanceRequestItem mRequestItem = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_service_layout, parent, false);
        }
        // Lookup view for data population
        TextView tvName = convertView.findViewById(R.id.txtTite);
        ImageView imgDelete = convertView.findViewById(R.id.imgDelete);

        tvName.setText(mRequestItem.getSubject());

//        if (isAdmin) {
//            imgDelete.setImageResource(R.drawable.ic_open_in_new_black_24dp);
//        } else {
//            imgDelete.setImageResource(R.drawable.ic_delete_forever_black_24dp);
//        }


        imgDelete.setImageResource(R.drawable.ic_open_in_new_black_24dp);

        imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // View the item from firebase
                fetchItem(mRequestItem.getKey());

                /*if (isAdmin) {
                    // View the item from firebase
                    fetchItem(mRequestItem.getKey());
                } else {
                    // Delete item from firebase
                    deleteItem(mRequestItem.getKey());
                }*/
            }
        });

        // Return the completed view to render on screen
        return convertView;
    }

    private void fetchItem(String firebaseKey) {

        mProgressDialog.show();
        final DatabaseReference servicePath = App.sInstance.getDbRoot().child("financeRequests/" + farmerId + "/" + firebaseKey);

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mProgressDialog.dismiss();

                try {
                    if (dataSnapshot.getValue() != null) {
                        FinanceRequestItem mRequestItem = dataSnapshot.getValue(FinanceRequestItem.class);
                        mRequestItem.setKey(dataSnapshot.getKey());
                        Log.e("mRequestItem", mRequestItem.getSubject());

                        Intent mIntent = new Intent(getContext(), ViewFinanceRequestItemActivity.class);
                        mIntent.putExtra("ITEM", mRequestItem);
                        getContext().startActivity(mIntent);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("onCancelled", databaseError.getMessage()); // Log errors
            }
        };
        servicePath.addListenerForSingleValueEvent(valueEventListener);
    }

    private void deleteItem(String firebaseKey) {

        mProgressDialog.show();
        final DatabaseReference requestPath = App.sInstance.getDbRoot().child("financeRequests/" + farmerId + "/" + firebaseKey);
        requestPath.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                mProgressDialog.dismiss();
                if (task.isSuccessful()) {
                    Toast.makeText(getContext(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Error! Please try again later", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
