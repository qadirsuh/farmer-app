package com.farmer.app.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.farmer.app.R;
import com.farmer.app.Utils;
import com.farmer.app.models.ChatWithAdmin;
import com.farmer.app.models.ServiceItem;

import java.util.ArrayList;

public class ChatReCyclerViewAdapter extends RecyclerView.Adapter<ChatReCyclerViewAdapter.ViewHolder> {

    private ArrayList<ChatWithAdmin> listData;

    private int viewTypeLeft = 0;
    private int viewTypeRight = 1;

    // RecyclerView recyclerView;
    public ChatReCyclerViewAdapter(ArrayList<ChatWithAdmin> listData) {
        this.listData = listData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem;
        if (viewType == viewTypeLeft) {
            listItem = layoutInflater.inflate(R.layout.chat_item_left, parent, false);
        } else {
            listItem = layoutInflater.inflate(R.layout.chat_item_right, parent, false);
        }
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ChatWithAdmin myListData = listData.get(position);
        holder.textView.setText(myListData.getMessage());
//        holder.imageView.setImageResource(listdata[position].getImgId());
//        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(view.getContext(),"click on item: "+myListData.getTitle(),Toast.LENGTH_LONG).show();
//            }
//        });
    }


    @Override
    public int getItemCount() {
        return listData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;
        public RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.show_message);
        }
    }

    @Override
    public int getItemViewType(int position) {
        // return super.getItemViewType(position);

        if (listData.get(position).getCreatedByUser().equalsIgnoreCase(Utils.getSharedPref(Utils.KEY_LOGIN_USER_NAME))) {
            return viewTypeRight;
        } else {
            return viewTypeLeft;
        }
    }
}

