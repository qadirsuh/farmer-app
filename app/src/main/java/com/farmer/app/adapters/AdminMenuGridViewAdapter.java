package com.farmer.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.farmer.app.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdminMenuGridViewAdapter extends BaseAdapter {

    private Context mContext;
    private final ArrayList mData;

    public AdminMenuGridViewAdapter(Context context, HashMap<String, Integer> map) {
        mContext = context;
        mData = new ArrayList();
        mData.addAll(map.entrySet());
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Map.Entry<String, Integer> getItem(int position) {
        return (Map.Entry) mData.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View gridViewAndroid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            gridViewAndroid = new View(mContext);
            gridViewAndroid = inflater.inflate(R.layout.gridview_item, null);
            TextView txt_title = (TextView) gridViewAndroid.findViewById(R.id.txt_title);
            ImageView img_icon = (ImageView) gridViewAndroid.findViewById(R.id.img_icon);

            Map.Entry<String, Integer> item = getItem(position);

            String tile = item.getKey();
            int img = item.getValue();

            txt_title.setText(tile);
            img_icon.setImageResource(img);

        } else {
            gridViewAndroid = (View) convertView;
        }

        return gridViewAndroid;
    }
}