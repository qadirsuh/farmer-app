package com.farmer.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.farmer.app.AdminLoginActivity;
import com.farmer.app.ChatWithAdminActivity;
import com.farmer.app.FinanceRequestListingActivity;
import com.farmer.app.R;
import com.farmer.app.models.ChatUsers;
import com.farmer.app.models.FinanceRequestUsers;

import java.util.ArrayList;

public class FinanceUserListReCyclerViewAdapter extends RecyclerView.Adapter<FinanceUserListReCyclerViewAdapter.ViewHolder> {

    private ArrayList<FinanceRequestUsers> listData;

    // RecyclerView recyclerView;
    public FinanceUserListReCyclerViewAdapter(ArrayList<FinanceRequestUsers> listData) {
        this.listData = listData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.chat_users_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(listItem, listData);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final FinanceRequestUsers myListData = listData.get(position);
        holder.textView.setText(myListData.getDisplayName());
//        holder.imageView.setImageResource(listdata[position].getImgId());
//        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(view.getContext(),"click on item: "+myListData.getTitle(),Toast.LENGTH_LONG).show();
//            }
//        });
    }


    @Override
    public int getItemCount() {
        return listData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imageView;
        public TextView textView;
        public RelativeLayout relativeLayout;
        private ArrayList<FinanceRequestUsers> listData;

        public ViewHolder(View itemView, ArrayList<FinanceRequestUsers> listData) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.txtUserDisplayName);
            itemView.setOnClickListener(this);
            this.listData = listData;
        }

        @Override
        public void onClick(View v) {
            int position = getLayoutPosition();
            FinanceRequestUsers requestUsers = listData.get(position);
            Context mContext = v.getContext();


            Intent mIntent = new Intent(mContext, FinanceRequestListingActivity.class);
            mIntent.putExtra("PREVIOUS_ACTIVITY", AdminLoginActivity.class.getSimpleName());
            mIntent.putExtra("USER_DISPLAY_NAME", requestUsers.getDisplayName());
            mIntent.putExtra("CHAT_WITH_USER_ID", requestUsers.getUserName());
            mContext.startActivity(mIntent);
        }
    }
}

