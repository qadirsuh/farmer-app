package com.farmer.app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.farmer.app.adapters.ItemsListAdapter;
import com.farmer.app.models.ServiceItem;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class AdminServiceListingActivity extends AppCompatActivity {

    private Intent mIntent;
    private TextView txtServiceTitle;
    private ListView serviceListView;

    private ProgressDialog mProgressDialog;

    private String selectedService;
    private String previousActivity;

    private Query uidRef;
    private ValueEventListener valueEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_service_listing);

        mProgressDialog = Utils.progressDialog("Loading...", this);

        txtServiceTitle = findViewById(R.id.txtServiceTitle);
        serviceListView = findViewById(R.id.serviceListView);

        mIntent = getIntent();
        selectedService = mIntent.getStringExtra("SELECTED_SERVICE");
        previousActivity = mIntent.getStringExtra("PREVIOUS_ACTIVITY");

        txtServiceTitle.setText(selectedService);

        FloatingActionButton fab = findViewById(R.id.fab);

        if (!previousActivity.equalsIgnoreCase(AdminLoginActivity.class.getSimpleName())) {
            fab.hide();
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent mIntent = new Intent(AdminServiceListingActivity.this, AddServiceItemActivity.class);
                mIntent.putExtra("SELECTED_SERVICE", selectedService);
                startActivity(mIntent);

            }
        });

        // Call get all services
        getAllServicesItems();
    }

    private void getAllServicesItems() {

        mProgressDialog.show();

        String service = selectedService.replace(" ", "_");

        final DatabaseReference servicePath = App.sInstance.getDbRoot().child("adminServices/" + service + "");
        uidRef = servicePath.orderByKey();
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mProgressDialog.dismiss();

                ArrayList<ServiceItem> serviceItems = new ArrayList<>();

                try {

                    if (dataSnapshot.getChildrenCount() == 0) {
                        Toast.makeText(AdminServiceListingActivity.this, "No items found!", Toast.LENGTH_SHORT).show();
                    }

                    Log.e("dataSnapshot count", dataSnapshot.getChildrenCount() + "");

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        ServiceItem mServiceItem = postSnapshot.getValue(ServiceItem.class);
                        mServiceItem.setKey(postSnapshot.getKey());
                        Log.e("mServiceItem", mServiceItem.getTitle());
                        serviceItems.add(mServiceItem);
                    }

                    Collections.reverse(serviceItems);

                    boolean isAdmin = false;
                    if (previousActivity.equalsIgnoreCase(AdminLoginActivity.class.getSimpleName())) {
                        isAdmin = true;
                    }

                    ItemsListAdapter mItemsListAdapter = new ItemsListAdapter(AdminServiceListingActivity.this, serviceItems, selectedService, isAdmin);
                    serviceListView.setAdapter(mItemsListAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("onCancelled", databaseError.getMessage()); // Log errors
            }
        };
        uidRef.addValueEventListener(valueEventListener);
    }
}
