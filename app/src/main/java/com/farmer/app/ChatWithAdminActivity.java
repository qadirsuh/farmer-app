package com.farmer.app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.farmer.app.adapters.ChatReCyclerViewAdapter;
import com.farmer.app.adapters.ItemsListAdapter;
import com.farmer.app.models.ChatWithAdmin;
import com.farmer.app.models.ServiceItem;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ChatWithAdminActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ChatReCyclerViewAdapter adapter;

    private TextView username;
    private EditText edMessageInput;
    private ImageButton btnSend;

    private ArrayList<ChatWithAdmin> chatItems = new ArrayList<>();

    private ProgressDialog mProgressDialog;

    private String farmerIdToChatWith;
    private Intent mIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_with_admin);

        mIntent = getIntent();

        mProgressDialog = Utils.progressDialog("Loading...", this);

        username = findViewById(R.id.username);
        edMessageInput = findViewById(R.id.edMessageInput);
        btnSend = findViewById(R.id.btnSend);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new ChatReCyclerViewAdapter(chatItems);
        recyclerView.setAdapter(adapter);

        if (Utils.getSharedPref(Utils.KEY_APP_TYPE).equalsIgnoreCase(Utils.KEY_ADMIN_APP)) {
            farmerIdToChatWith = mIntent.getStringExtra("CHAT_WITH_USER_ID");
            Log.e("farmerIdToChatWith", farmerIdToChatWith);
            String farmerDisplayName = mIntent.getStringExtra("USER_DISPLAY_NAME");
            username.setText("Chat with \"" + farmerDisplayName + "\"");
        } else {
            farmerIdToChatWith = Utils.getSharedPref(Utils.KEY_LOGIN_USER_NAME);
        }

        // Fetch messages from firebase database
        getAllChatDbMessage();

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String message = edMessageInput.getText().toString().trim();
                if (!message.isEmpty()) {

                    Map<String, Object> messageData = new HashMap<>();
                    messageData.put("createdAt", System.currentTimeMillis());
                    messageData.put("message", message);
                    /*if (Utils.getSharedPref(Utils.KEY_APP_TYPE).equalsIgnoreCase(Utils.KEY_ADMIN_APP)) {
                        messageData.put("isAdmin", true);
                    } else {
                        messageData.put("isAdmin", false);
                    }*/
                    messageData.put("createdByUser", Utils.getSharedPref(Utils.KEY_LOGIN_USER_NAME));
                    addMessageInDb(messageData);
                }
            }
        });
    }

    private void addMessageInDb(final Map<String, Object> messageData) {

        edMessageInput.setText("");
        btnSend.setEnabled(false);
        btnSend.setAlpha(0.5f);


        final DatabaseReference chatWithAdminPath = App.sInstance.getDbRoot().child("chatWithAdmin/" + farmerIdToChatWith + "");
        DatabaseReference messageId = chatWithAdminPath.push();
        messageId.setValue(messageData).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (!task.isSuccessful()) {
                    edMessageInput.setText(messageData.get("message").toString());
                    Toast.makeText(ChatWithAdminActivity.this, "Sending failed! Please try again later.", Toast.LENGTH_LONG).show();
                }

                btnSend.setEnabled(true);
                btnSend.setAlpha(1f);
            }
        });
    }

    private Query uidRef;
    private ValueEventListener valueEventListener;

    private void getAllChatDbMessage() {

        mProgressDialog.show();

        final DatabaseReference adminChatPath = App.sInstance.getDbRoot().child("chatWithAdmin/" + farmerIdToChatWith + "");
        uidRef = adminChatPath.orderByKey();
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mProgressDialog.dismiss();
                chatItems.clear();

                try {

                    if (dataSnapshot.getChildrenCount() == 0) {
                        Toast.makeText(ChatWithAdminActivity.this, "Chat is empty!", Toast.LENGTH_SHORT).show();
                    } else if (dataSnapshot.getChildrenCount() == 1) {

                        // Add Entry in Chat List User Display Name
                        Map<String, Object> userEntryData = new HashMap<>();
                        userEntryData.put("displayName", Utils.getSharedPref(Utils.KEY_LOGIN_USER_DISPLAY_NAME));
                        userEntryData.put("lastModified", System.currentTimeMillis());
                        userEntryData.put("userName", Utils.getSharedPref(Utils.KEY_LOGIN_USER_NAME));

                        addUserEntryInChatListForAdmin(userEntryData);
                    }

                    Log.e("dataSnapshot count", dataSnapshot.getChildrenCount() + "");

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        ChatWithAdmin mChatObj = postSnapshot.getValue(ChatWithAdmin.class);
                        mChatObj.setKey(postSnapshot.getKey());
                        Log.e("message", mChatObj.getMessage());
                        chatItems.add(mChatObj);
                    }

                    adapter.notifyDataSetChanged();
                    recyclerView.setAdapter(adapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("onCancelled", databaseError.getMessage()); // Log errors
            }
        };
        uidRef.addValueEventListener(valueEventListener);
    }

    private void addUserEntryInChatListForAdmin(Map<String, Object> userEntryData) {
        final DatabaseReference chatWithAdminPath = App.sInstance.getDbRoot().child("chatWithAdmin/chatListUser/" + farmerIdToChatWith + "");
        chatWithAdminPath.setValue(userEntryData);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        uidRef.removeEventListener(valueEventListener);
    }
}
