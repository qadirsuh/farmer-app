package com.farmer.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.farmer.app.adapters.AdminMenuGridViewAdapter;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class DashboardActivity extends AppCompatActivity {

    GridView androidGridView;
    HashMap<String, Integer> mMap = new LinkedHashMap<>();

    private Intent mIntent;
    private String previousActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_dashboard);

        androidGridView = findViewById(R.id.gridview_menu);

        mIntent = getIntent();
        previousActivity = mIntent.getStringExtra("PREVIOUS_ACTIVITY");

        setGridMenu();
        androidGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int i, long id) {
                Map.Entry<String, Integer> item = (Map.Entry<String, Integer>) parent.getItemAtPosition(i);

                String selectedService = item.getKey();
                Log.e("Clicked", selectedService);

                if (previousActivity.equalsIgnoreCase(AdminLoginActivity.class.getSimpleName())) {

                    if (!selectedService.equalsIgnoreCase(Constants.KEY_ProjectFinanceRequests) && !selectedService.equalsIgnoreCase(Constants.KEY_AdviceToFarmer)) {
                        Intent mIntent = new Intent(DashboardActivity.this, AdminServiceListingActivity.class);
                        mIntent.putExtra("SELECTED_SERVICE", selectedService);
                        mIntent.putExtra("PREVIOUS_ACTIVITY", previousActivity);
                        startActivity(mIntent);
                    } else {

                        if (selectedService.equalsIgnoreCase(Constants.KEY_ProjectFinanceRequests)) {

                            Intent mIntent = new Intent(DashboardActivity.this, FinanceRequestUsersListActivity.class);
                            mIntent.putExtra("SELECTED_SERVICE", selectedService);
                            mIntent.putExtra("PREVIOUS_ACTIVITY", previousActivity);
                            startActivity(mIntent);

                        } else if (selectedService.equalsIgnoreCase(Constants.KEY_AdviceToFarmer)) {

                            Intent mIntent = new Intent(DashboardActivity.this, ChatUsersListActivity.class);
                            mIntent.putExtra("PREVIOUS_ACTIVITY", previousActivity);
                            startActivity(mIntent);
                        }
                    }

                } else {

                    if (!selectedService.equalsIgnoreCase(Constants.KEY_ProjectFinanceRequests) && !selectedService.equalsIgnoreCase(Constants.KEY_expertAdvice)) {
                        Intent mIntent = new Intent(DashboardActivity.this, AdminServiceListingActivity.class);
                        mIntent.putExtra("SELECTED_SERVICE", selectedService);
                        mIntent.putExtra("PREVIOUS_ACTIVITY", previousActivity);
                        startActivity(mIntent);
                    } else {

                        if (selectedService.equalsIgnoreCase(Constants.KEY_ProjectFinanceRequests)) {

                            Intent mIntent = new Intent(DashboardActivity.this, FinanceRequestListingActivity.class);
                            mIntent.putExtra("SELECTED_SERVICE", selectedService);
                            mIntent.putExtra("PREVIOUS_ACTIVITY", previousActivity);
                            startActivity(mIntent);

                        } else if (selectedService.equalsIgnoreCase(Constants.KEY_expertAdvice)) {
                            Intent mIntent = new Intent(DashboardActivity.this, ChatWithAdminActivity.class);
                            mIntent.putExtra("PREVIOUS_ACTIVITY", previousActivity);
                            startActivity(mIntent);
                        }
                    }

                }
            }
        });
    }

    private void setGridMenu() {

        mMap.put(Constants.KEY_MinistryPolicies, R.drawable.ic_ministory_policy);
        mMap.put(Constants.KEY_LatestNews, R.drawable.ic_ministory_policy);
        mMap.put(Constants.KEY_CropAdvisoryServices, R.drawable.ic_ministory_policy);
        mMap.put(Constants.KEY_MarketTrends, R.drawable.ic_ministory_policy);
        if (Utils.getSharedPref(Utils.KEY_APP_TYPE).equalsIgnoreCase(Utils.KEY_ADMIN_APP)) {
            mMap.put(Constants.KEY_AdviceToFarmer, R.drawable.ic_ministory_policy);
        } else {
            mMap.put(Constants.KEY_expertAdvice, R.drawable.ic_ministory_policy);
        }
        mMap.put(Constants.KEY_ProjectFinanceRequests, R.drawable.ic_ministory_policy);
        mMap.put(Constants.KEY_WeatherForecast, R.drawable.ic_ministory_policy);

        AdminMenuGridViewAdapter menuGridViewAdapter = new AdminMenuGridViewAdapter(DashboardActivity.this, mMap);
        androidGridView.setAdapter(menuGridViewAdapter);
    }
}
