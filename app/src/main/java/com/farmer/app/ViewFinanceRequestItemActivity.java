package com.farmer.app;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.farmer.app.models.FinanceRequestItem;
import com.farmer.app.models.ServiceItem;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.regex.Pattern;

public class ViewFinanceRequestItemActivity extends AppCompatActivity {

    private Intent mIntent;


    private TextView txt_subject;
    private TextView txt_message;
    private TextView txt_status;
    private Button btn_back, btnViewLoanFile, btnApprove, btnReject;
    private LinearLayout adminSection;

    private ProgressDialog mProgressDialog;


    //Firebase storage
    FirebaseStorage storage;
    StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_finance_request_item);

        mProgressDialog = Utils.progressDialog("Loading...", this);

        txt_subject = findViewById(R.id.txt_subject);
        txt_message = findViewById(R.id.txt_message);
        txt_status = findViewById(R.id.txt_status);
        btnViewLoanFile = findViewById(R.id.btnViewLoanFile);
        btnApprove = findViewById(R.id.btnApprove);
        btnReject = findViewById(R.id.btnReject);
        adminSection = findViewById(R.id.adminSection);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        btn_back = findViewById(R.id.btn_back);

        if (Utils.getSharedPref(Utils.KEY_APP_TYPE).equalsIgnoreCase(Utils.KEY_ADMIN_APP)) {
            adminSection.setVisibility(View.VISIBLE);
        } else {
            adminSection.setVisibility(View.GONE);
        }

        mIntent = getIntent();
        final FinanceRequestItem financeRequestItem = (FinanceRequestItem) mIntent.getSerializableExtra("ITEM");

        txt_subject.setText(financeRequestItem.getSubject());
        txt_message.setMovementMethod(new ScrollingMovementMethod());
        txt_message.setMovementMethod(LinkMovementMethod.getInstance());
        txt_message.setText(financeRequestItem.getMessage());
        Linkify.addLinks(txt_message, Linkify.ALL);

        txt_status.setText(financeRequestItem.getStatus());

        refreshStatusTextColor();

        btnViewLoanFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StorageReference fileRef = storageReference.child(financeRequestItem.getDownloadUrl());
                downloadToLocalFileAndView(fileRef);
            }
        });

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approveFinanceRequest(financeRequestItem.getKey(), financeRequestItem.getCreatedByUser());
            }
        });

        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rejectFinanceRequest(financeRequestItem.getKey(), financeRequestItem.getCreatedByUser());
            }
        });

    }

    private void approveFinanceRequest(String requestKey, String farmerId) {

        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();
        final String status = "APPROVED";
        final DatabaseReference servicePath = App.sInstance.getDbRoot().child("financeRequests/" + farmerId + "/" + requestKey + "/status");
        servicePath.setValue(status).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                mProgressDialog.dismiss();
                Toast.makeText(ViewFinanceRequestItemActivity.this, "Approved successfully", Toast.LENGTH_SHORT).show();
                txt_status.setText(status);
                refreshStatusTextColor();
            }
        });
    }

    private void rejectFinanceRequest(String requestKey, String farmerId) {

        mProgressDialog.setMessage("Loading...");
        mProgressDialog.show();

        final String status = "REJECTED";
        final DatabaseReference servicePath = App.sInstance.getDbRoot().child("financeRequests/" + farmerId + "/" + requestKey + "/status");
        servicePath.setValue(status).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                mProgressDialog.dismiss();
                Toast.makeText(ViewFinanceRequestItemActivity.this, "Rejected successfully", Toast.LENGTH_SHORT).show();
                txt_status.setText(status);
                refreshStatusTextColor();
            }
        });
    }


    private void refreshStatusTextColor() {

        String status = txt_status.getText().toString();
        if (status.equalsIgnoreCase("PENDING")) {
            txt_status.setTextColor(Color.parseColor("#F9BC48"));
        } else if (status.equalsIgnoreCase("APPROVED")) {
            txt_status.setTextColor(Color.parseColor("#6BA833"));
        } else {
            txt_status.setTextColor(Color.parseColor("#CB0043"));
        }
    }


    private void downloadToLocalFileAndView(final StorageReference fileRef) {


        if (fileRef != null) {

            mProgressDialog.show();

            fileRef.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                @Override
                public void onSuccess(StorageMetadata storageMetadata) {

                    final String mimeType = storageMetadata.getContentType();
                    String fileNameWithExtension = storageMetadata.getName();

                    Log.e("fileNameWithExtension", fileNameWithExtension);
                    String[] splitFile = fileNameWithExtension.split(Pattern.quote("."));

                    String fileName = splitFile[0];
                    String fileExtension = splitFile[1];

                    try {

                        final File localFile = File.createTempFile(fileName, "." + fileExtension);

                        fileRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                                mProgressDialog.dismiss();

                                Uri uri = FileProvider.getUriForFile(ViewFinanceRequestItemActivity.this, "be.myapplication", localFile);

                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                intent.setDataAndType(uri, mimeType);

                                try {
                                    startActivity(intent);
                                } catch (ActivityNotFoundException e) {
                                    e.printStackTrace();
                                    Toast.makeText(ViewFinanceRequestItemActivity.this, "Unable to view the file", Toast.LENGTH_SHORT).show();
                                }

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                mProgressDialog.dismiss();
                                Toast.makeText(ViewFinanceRequestItemActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }).addOnProgressListener(new OnProgressListener<FileDownloadTask.TaskSnapshot>() {
                            @Override
                            public void onProgress(FileDownloadTask.TaskSnapshot taskSnapshot) {
                                // progress percentage
                                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                                // percentage in progress dialog
                                mProgressDialog.setMessage("Downloaded " + ((int) progress) + "%...");
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {

                }
            });

        } else {
            Toast.makeText(ViewFinanceRequestItemActivity.this, "Upload file before downloading", Toast.LENGTH_LONG).show();
        }
    }

}
