package com.farmer.app;

public class Constants {

    public static String KEY_MinistryPolicies = "Ministry Policies";
    public static String KEY_LatestNews = "Latest News";
    public static String KEY_CropAdvisoryServices = "Crop Advisory Services";
    public static String KEY_MarketTrends = "Market Trends";
    public static String KEY_AdviceToFarmer = "Advice to Farmer";
    public static String KEY_expertAdvice = "Expert Advice";
    public static String KEY_ProjectFinanceRequests = "Project/Finance Requests";
    public static String KEY_WeatherForecast = "Weather Forecast";

}
