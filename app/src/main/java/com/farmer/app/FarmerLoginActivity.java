package com.farmer.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import static android.text.Html.fromHtml;

public class FarmerLoginActivity extends AppCompatActivity {

    private EditText editTextEmail;
    private EditText editTextPassword;
    private TextInputLayout textInputLayoutEmail;
    private TextInputLayout textInputLayoutPassword;
    private Button buttonLogin;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_login);
        initCreateAccountTextView();
        initViews();

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Check user input is correct or not
                if (validate()) {

                    mProgressDialog.show();

                    //Get values from EditText fields
                    String userName = editTextEmail.getText().toString();
                    String password = editTextPassword.getText().toString();

                    checkUserLogin(userName, password);
                }
            }
        });
    }

    private void checkUserLogin(final String userName, final String userPassword) {

        DatabaseReference ref = App.sInstance.getDbRoot().child("farmerUsers/" + userName);
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mProgressDialog.dismiss();
                try {

                    if (dataSnapshot.getValue() != null) {
                        String dbUserName = dataSnapshot.child("userName").getValue().toString();
                        String dbPassword = dataSnapshot.child("password").getValue().toString();
                        String userDisplayName = dataSnapshot.child("fullName").getValue().toString();
                        Log.e("dbPassword", dbPassword);
                        if (dbPassword.equals(userPassword)) {
                            // Goto menu Screen
                            Toast.makeText(FarmerLoginActivity.this, "Login success", Toast.LENGTH_SHORT).show();
                            Utils.saveSharedPref(Utils.KEY_LOGIN_USER_NAME, dbUserName);
                            Utils.saveSharedPref(Utils.KEY_LOGIN_PASSWORD, dbPassword);
                            Utils.saveSharedPref(Utils.KEY_LOGIN_USER_DISPLAY_NAME, userDisplayName);
                            Intent mIntent = new Intent(FarmerLoginActivity.this, DashboardActivity.class);
                            mIntent.putExtra("PREVIOUS_ACTIVITY", FarmerLoginActivity.class.getSimpleName());
                            startActivity(mIntent);
                            finish();

                        } else {
                            Toast.makeText(FarmerLoginActivity.this, "Invalid user name or password!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(FarmerLoginActivity.this, "Invalid user name or password!", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("onCancelled", databaseError.getMessage()); // Log errors
            }
        };
        ref.addListenerForSingleValueEvent(valueEventListener);
    }

    // this method is used to connect XML views to its Objects
    private void initViews() {

        mProgressDialog = Utils.progressDialog("Loading...", this);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        textInputLayoutEmail = (TextInputLayout) findViewById(R.id.textInputLayoutEmail);
        textInputLayoutPassword = (TextInputLayout) findViewById(R.id.textInputLayoutPassword);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);
    }

    //this method used to set Create account TextView text and click event( maltipal colors
    // for TextView yet not supported in Xml so i have done it programmatically)
    private void initCreateAccountTextView() {
        TextView textViewCreateAccount = (TextView) findViewById(R.id.textViewCreateAccount);
        textViewCreateAccount.setText(fromHtml("<font color='#000000'>I don't have account yet. </font><font color='#0c0099'>create one</font>"));
        textViewCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FarmerLoginActivity.this, FarmerRegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    // This method is used to validate input given by user
    public boolean validate() {
        boolean valid = false;

        //Get values from EditText fields
        String Email = editTextEmail.getText().toString();
        String Password = editTextPassword.getText().toString();

        //Handling validation for Password field
        if (Email.isEmpty()) {
            valid = false;
            textInputLayoutEmail.setError("Field required!");
        } else if (Password.isEmpty()) {
            textInputLayoutPassword.setError("Field required!");
            valid = false;
        } else {
            textInputLayoutEmail.setError(null);
            textInputLayoutEmail.setError(null);
            valid = true;
        }

        return valid;
    }
}
