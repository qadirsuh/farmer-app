package com.farmer.app;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.jaiselrahman.filepicker.activity.FilePickerActivity;
import com.jaiselrahman.filepicker.config.Configurations;
import com.jaiselrahman.filepicker.model.MediaFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class FarmerFinanceRequestActivity extends AppCompatActivity {

    private EditText txtSubject;
    private EditText txtMessage;
    private Button btnChooseFile;
    private Button btnSubmit;
    private TextView txtFilePath;

    private ProgressDialog mProgressDialog;
    private Uri filePathToUpload;
    private String fileSuffixName;
    private int FILE_PICK_REQUEST_CODE = 100;
    private Map<String, Object> financeReqData;

    //Firebase storage
    FirebaseStorage storage;
    StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_finance_request);

        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        mProgressDialog = Utils.progressDialog("Loading...", this);

        txtSubject = findViewById(R.id.edSubject);
        txtMessage = findViewById(R.id.edMessage);
        txtFilePath = findViewById(R.id.txtFilePath);
        btnChooseFile = findViewById(R.id.btnChooseFile);
        btnSubmit = findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String subject = txtSubject.getText().toString();
                String message = txtMessage.getText().toString();

                if (subject.isEmpty()) {
                    txtSubject.setError("* Required field");
                } else if (message.isEmpty()) {
                    txtMessage.setError("* Required field");
                } else if (filePathToUpload == null) {
                    Toast.makeText(FarmerFinanceRequestActivity.this, "Upload loan/finance request file!", Toast.LENGTH_SHORT).show();
                    txtFilePath.setTextColor(Color.parseColor("#FF0000"));
                } else {

//                    btnSubmit.setEnabled(false);
                    financeReqData = new HashMap<>();
                    financeReqData.put("createdAt", System.currentTimeMillis());
                    financeReqData.put("subject", subject);
                    financeReqData.put("message", message);
                    financeReqData.put("createdByUser", Utils.getSharedPref(Utils.KEY_LOGIN_USER_NAME));
                    financeReqData.put("filePath", filePathToUpload.toString());
                    financeReqData.put("status", "PENDING");

                    addUserToUsersListForAdmin();

                    uploadFileDb(financeReqData);
                }
            }
        });

        btnChooseFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(FarmerFinanceRequestActivity.this, FilePickerActivity.class);
                intent.putExtra(FilePickerActivity.CONFIGS, new Configurations.Builder()
                        .setCheckPermission(true)
                        //.setShowImages(true)
                        .enableImageCapture(true)
                        .setSingleChoiceMode(true)
                        .setShowFiles(true)
                        .setSkipZeroSizeFiles(true)
                        .build());
                startActivityForResult(intent, FILE_PICK_REQUEST_CODE);

            }
        });

    }


    private void addUserToUsersListForAdmin() {

        String farmerId = Utils.getSharedPref(Utils.KEY_LOGIN_USER_NAME);

        // Add Entry in Chat List User Display Name
        Map<String, Object> userEntryData = new HashMap<>();
        userEntryData.put("displayName", Utils.getSharedPref(Utils.KEY_LOGIN_USER_DISPLAY_NAME));
        userEntryData.put("lastModified", System.currentTimeMillis());
        userEntryData.put("userName", farmerId);

        final DatabaseReference chatWithAdminPath = App.sInstance.getDbRoot().child("financeRequests/financeListUser/" + farmerId + "");
        chatWithAdminPath.setValue(userEntryData);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FILE_PICK_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                ArrayList<MediaFile> files = data.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES);

                for (MediaFile file : files) {
                    txtFilePath.setTextColor(Color.parseColor("#606060"));
                    txtFilePath.setText(file.getName());
                    fileSuffixName = file.getName();
                    filePathToUpload = file.getUri();

                    /*Log.e("getName", file.getName());
                    Log.e("getMimeType", file.getMimeType());
                    Log.e("getPath", file.getPath());
                    Log.e("getSize", file.getSize()+"");
                    Log.e("getBucketId", file.getBucketId()+"");
                    Log.e("getBucketName", file.getBucketName()+"");
                    Log.e("getDuration", file.getDuration()+"");
                    Log.e("getId", file.getId()+"");
                    Log.e("getThumbnail", file.getThumbnail()+"");
                    Log.e("getUri", file.getUri()+"");
                    Log.e("getWidth", file.getWidth()+"");
                    Log.e("getHeight", file.getHeight()+"");
                    Log.e("getMediaType", file.getMediaType()+"");*/
                }
            }
        }
    }

    private void addRequestToDB(final Map<String, Object> data) {

        String farmerId = Utils.getSharedPref(Utils.KEY_LOGIN_USER_NAME);
        final DatabaseReference servicePath = App.sInstance.getDbRoot().child("financeRequests/" + farmerId + "");
        DatabaseReference newFinanceReqId = servicePath.push();
        newFinanceReqId.setValue(data).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                mProgressDialog.dismiss();

                if (task.isSuccessful()) {
                    Toast.makeText(FarmerFinanceRequestActivity.this, "Finance request sent!", Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    Toast.makeText(FarmerFinanceRequestActivity.this, "Failed! Please try again later", Toast.LENGTH_LONG).show();
                    btnSubmit.setEnabled(true);
                }
            }
        });
    }


    private void uploadFileDb(final Map<String, Object> data) {

        mProgressDialog = Utils.progressDialog("Uploading...", this);
        mProgressDialog.show();


        Log.e("fileSuffixName", fileSuffixName);

        final StorageReference ref = storageReference.child("images/" + fileSuffixName);

        ref.putFile(Uri.parse(data.get("filePath").toString()))
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // send Request after uploading the file
                        data.put("downloadUrl", taskSnapshot.getMetadata().getPath());
                        addRequestToDB(data);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mProgressDialog.dismiss();
                        Toast.makeText(FarmerFinanceRequestActivity.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                .getTotalByteCount());
                        mProgressDialog.setMessage("Uploading... " + (int) progress + "%");
                    }
                });
    }

}
