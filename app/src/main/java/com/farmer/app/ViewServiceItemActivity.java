package com.farmer.app;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import com.farmer.app.R;
import com.farmer.app.models.ServiceItem;

public class ViewServiceItemActivity extends AppCompatActivity {

    private Intent mIntent;


    private TextView txt_title;
    private TextView txt_content;
    private Button btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_service_item);

        txt_title = findViewById(R.id.txt_title);
        txt_content = findViewById(R.id.txt_content);
        btn_back = findViewById(R.id.btn_back);

        mIntent = getIntent();
        ServiceItem serviceItem = (ServiceItem) mIntent.getSerializableExtra("ITEM");

        txt_title.setText(serviceItem.getTitle());
        txt_content.setMovementMethod(new ScrollingMovementMethod());
        txt_content.setMovementMethod(LinkMovementMethod.getInstance());
        txt_content.setText(serviceItem.getContent());
        Linkify.addLinks(txt_content, Linkify.ALL);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
