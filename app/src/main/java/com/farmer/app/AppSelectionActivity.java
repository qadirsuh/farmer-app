package com.farmer.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class AppSelectionActivity extends AppCompatActivity {

    private Button btnFarmerApp, btnAdminApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_selection);

        btnFarmerApp = findViewById(R.id.btnFarmerApp);
        btnAdminApp = findViewById(R.id.btnAdminApp);

        btnFarmerApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.saveSharedPref(Utils.KEY_APP_TYPE, Utils.KEY_FARMER_APP);
                startActivity(new Intent(AppSelectionActivity.this, FarmerLoginActivity.class));
                finish();
            }
        });

        btnAdminApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.saveSharedPref(Utils.KEY_APP_TYPE, Utils.KEY_ADMIN_APP);
                startActivity(new Intent(AppSelectionActivity.this, AdminLoginActivity.class));
                finish();
            }
        });
    }
}
